const sqlite3 = require("sqlite3").verbose();
const db = new sqlite3.Database("comments.db");
const util = require("util");

const run = util.promisify(db.run.bind(db));
const all = util.promisify(db.all.bind(db));
const get = util.promisify(db.get.bind(db));

const bootstrap = () => {
  return run(`create table if not exists comments (id integer primary key,
                                                   name text not null,
                                                   body text not null,
                                                   timestamp date not null);`);
};

const getComments = () => {
  return all("select* from comments order by timestamp desc;");
};

const addComment = comment => {
  const { name, body } = comment;
  const timestamp = Date.now();
  return run(`insert into comments(name, body, timestamp) values (?,?,?);`, [
    name,
    body,
    timestamp
  ]);
};

module.exports = {
  bootstrap,
  addComment,
  getComments
};
