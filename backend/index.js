//CRUD TEMEPLATE
const express = require("express");
const app = express();
const port = 3001;
const bodyParser = require("body-parser");
const dbservice = require("./dbservice.js");

app.use(bodyParser.json({ type: "*/*" })); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

//express.static(root, [options])
//http://localhost:3001/test.html
app.use(express.static("assets"));

// app.METHOD(PATH, HANDLER)

app.get("/comments", async (req, res) => {
  const comments = await dbservice.getComments();
  res.set("Access-Control-Allow-Origin", "*");
  res.json(comments);
});

app.post("/comments", async (req, res) => {
  await dbservice.addComment(req.body);
  res.set("Access-Control-Allow-Origin", "*");
  res.json({ created: true });
});

// app.put("/comments", (req, res) => res.json({ updated: true }));
// app.delete("/comments", (req, res) => res.json({ deleted: true }));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

dbservice.bootstrap();
