import React, { Component } from "react";
import Guestform from "./components/Guestform";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="container">
        <Guestform />
      </div>
    );
  }
}

export default App;
