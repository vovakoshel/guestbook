import React, { Component } from "react";
import Comment from "./Comment";

class Guestform extends Component {
  state = {
    name: "",
    comment: "",
    comments: []
  };

  fetchAllComments = async () => {
    const comments = await fetch("http://localhost:3001/comments").then(res =>
      res.json()
    );
    return comments;
  };

  async componentDidMount() {
    const comments = await this.fetchAllComments();
    this.setState({
      comments: comments
    });
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = async e => {
    e.preventDefault();

    await fetch("http://localhost:3001/comments", {
      method: "POST",
      body: JSON.stringify({
        name: this.state.name,
        body: this.state.comment
      })
    });

    const comments = await this.fetchAllComments();

    this.setState({
      name: "",
      comment: "",
      comments: comments
    });
  };

  render() {
    const { name, comment, comments } = this.state;
    return (
      <div>
        <div className="card mb-3">
          <div className="card-header">Guestform</div>
          <div className="card-body">
            <form onSubmit={this.onSubmit}>
              <div className="form-group ">
                <label htmlFor="name">Name</label>
                <input
                  type="text"
                  name="name"
                  className="form-control"
                  placeholder="Enter Name..."
                  value={name}
                  onChange={this.onChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="your comment">Your Comment</label>
                <textarea
                  name="comment"
                  className="form-control"
                  value={comment}
                  placeholder="Enter Comment..."
                  onChange={this.onChange}
                />
              </div>
              <input
                type="submit"
                value="Add Comment"
                className="btn btn-light btn-block"
              />
            </form>
          </div>
        </div>

        <div>
          {comments.map(comment => (
            <Comment
              key={comment.id}
              name={comment.name}
              body={comment.body}
              timestamp={comment.timestamp}
            />
          ))}
        </div>
      </div>
    );
  }
}
export default Guestform;
