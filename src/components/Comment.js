import React from "react";

export default function Comment(props) {
  return (
    <div>
      <div className="car">
        <div className="card-body">
          <h3>{props.name}</h3>
          <p>{props.body}</p>
          <span>{new Date(props.timestamp).toLocaleString("ru-RU")}</span>
        </div>
      </div>
    </div>
  );
}
